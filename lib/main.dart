import 'package:app_rockets_spacex/src/pages/home_page.dart';
import 'package:custom_splash/custom_splash.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  void statusBarTransparent() {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: Colors.transparent),
    );
  }

  void notRotate() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    this.statusBarTransparent();
    this.notRotate();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'SpaceX',
      home: CustomSplash(
          imagePath: 'assets/images/astronaut.png',
          backGroundColor: Colors.black,
          animationEffect: 'zoom-in',
          logoSize: 700,
          home: HomePage(),
          duration: 2600,
          type: CustomSplashType.StaticDuration),
    );
  }
}
