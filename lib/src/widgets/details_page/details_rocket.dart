import 'package:app_rockets_spacex/src/service/https/request_rockets.dart';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';

class DetailsRocketWidget extends StatefulWidget {
  final String idRocket;
  DetailsRocketWidget({this.idRocket});
  @override
  _DetailsRocketWidgetState createState() => _DetailsRocketWidgetState();
}

class _DetailsRocketWidgetState extends State<DetailsRocketWidget> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      builder: (context, projectSnap) {
        if (projectSnap.connectionState == ConnectionState.none &&
            projectSnap.hasData == null) {
          return Container(
            color: Colors.black,
            child: Center(
                child: Column(
              children: <Widget>[
                Text(
                  'Error en el servidor',
                  style: TextStyle(color: Colors.white, letterSpacing: 2.0),
                )
              ],
            )),
          );
        } else if (projectSnap.connectionState == ConnectionState.waiting) {
          return Container(
              color: Colors.black,
              child: Center(
                  child: SizedBox(
                width: 30,
                child: LoadingIndicator(
                  indicatorType: Indicator.circleStrokeSpin,
                  color: Colors.white,
                ),
              )));
        } else if (projectSnap.hasData) {
          String firsDate =
              DateFormat('yMMMMd').format(projectSnap.data.firstFlight);
          return Container(
            child: Stack(
              children: <Widget>[
                CachedNetworkImage(
                  fit: BoxFit.fitHeight,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 2 + 20,
                  imageUrl: projectSnap.data.flickrImages[0],
                  placeholder: (context, url) => Container(
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 20,
                      child: LoadingIndicator(
                        indicatorType: Indicator.semiCircleSpin,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  errorWidget: (context, url, error) => SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: 20,
                    child: Center(
                      child: Icon(Icons.airplanemode_active),
                    ),
                  ),
                ),
                DraggableScrollableSheet(
                    initialChildSize: 0.49,
                    minChildSize: 0.48,
                    maxChildSize: 0.50,
                    builder: (BuildContext context,
                        ScrollController scrollController) {
                      return Container(
                        decoration: BoxDecoration(
                          color: Colors.black,
                          border: Border.all(width: 0.2),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30),
                          ),
                        ),
                        child: Container(
                          child: ListView(
                              shrinkWrap: true,
                              controller: scrollController,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(top: 20, left: 20),
                                  child: AutoSizeText(
                                    projectSnap.data.rocketName,
                                    style: TextStyle(
                                        fontSize: 13,
                                        color: Colors.white,
                                        fontFamily: 'Lato',
                                        letterSpacing: 3.0),
                                    minFontSize: 10,
                                    textScaleFactor: 1.2,
                                    textAlign: TextAlign.justify,
                                    stepGranularity: 2,
                                    maxLines: 1,
                                  ),
                                ),
                                Divider(
                                  color: Colors.white,
                                  indent: 20.0,
                                  endIndent: 20.0,
                                ),
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(left: 20),
                                        child: Icon(
                                          Icons.date_range,
                                          size: 17,
                                          color: Colors.white,
                                        ),
                                      ),
                                      Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.only(
                                                  top: 10, left: 20),
                                              child: AutoSizeText(
                                                'First Flight',
                                                style: TextStyle(
                                                    fontSize: 10,
                                                    color: Colors.white,
                                                    fontFamily: 'Lato',
                                                    letterSpacing: 3.0),
                                                minFontSize: 10,
                                                textScaleFactor: 1,
                                                textAlign: TextAlign.justify,
                                                stepGranularity: 1,
                                                maxLines: 1,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                  top: 4, left: 20),
                                              child: AutoSizeText(
                                                firsDate,
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.white,
                                                    fontFamily: 'Lato',
                                                    letterSpacing: 3.0),
                                                minFontSize: 10,
                                                textScaleFactor: 1,
                                                textAlign: TextAlign.justify,
                                                stepGranularity: 2,
                                                maxLines: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Divider(
                                  color: Colors.white,
                                  indent: 20.0,
                                  endIndent: 20.0,
                                ),
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(left: 20),
                                        child: Icon(
                                          Icons.multiline_chart,
                                          size: 17,
                                          color: Colors.white,
                                        ),
                                      ),
                                      Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.only(
                                                  top: 10, left: 20),
                                              child: AutoSizeText(
                                                'Height',
                                                style: TextStyle(
                                                    fontSize: 10,
                                                    color: Colors.white,
                                                    fontFamily: 'Lato',
                                                    letterSpacing: 3.0),
                                                minFontSize: 10,
                                                textScaleFactor: 1,
                                                textAlign: TextAlign.justify,
                                                stepGranularity: 1,
                                                maxLines: 1,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                  top: 4, left: 20),
                                              child: AutoSizeText(
                                                projectSnap.data.height.meters
                                                        .toString() +
                                                    ' m' +
                                                    " / " +
                                                    projectSnap.data.height.feet
                                                        .toString() +
                                                    ' ft',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.white,
                                                    fontFamily: 'Lato',
                                                    letterSpacing: 3.0),
                                                minFontSize: 10,
                                                textScaleFactor: 1,
                                                textAlign: TextAlign.justify,
                                                stepGranularity: 2,
                                                maxLines: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(left: 20),
                                        child: Icon(
                                          Icons.multiline_chart,
                                          size: 17,
                                          color: Colors.black,
                                        ),
                                      ),
                                      Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.only(
                                                  top: 10, left: 20),
                                              child: AutoSizeText(
                                                'Diameter',
                                                style: TextStyle(
                                                    fontSize: 10,
                                                    color: Colors.white,
                                                    fontFamily: 'Lato',
                                                    letterSpacing: 3.0),
                                                minFontSize: 10,
                                                textScaleFactor: 1,
                                                textAlign: TextAlign.justify,
                                                stepGranularity: 1,
                                                maxLines: 1,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                  top: 4, left: 20),
                                              child: AutoSizeText(
                                                projectSnap.data.diameter.meters
                                                        .toString() +
                                                    ' m' +
                                                    " / " +
                                                    projectSnap
                                                        .data.diameter.feet
                                                        .toString() +
                                                    ' ft',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.white,
                                                    fontFamily: 'Lato',
                                                    letterSpacing: 3.0),
                                                minFontSize: 10,
                                                textScaleFactor: 1,
                                                textAlign: TextAlign.justify,
                                                stepGranularity: 2,
                                                maxLines: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(left: 20),
                                        child: Icon(
                                          Icons.multiline_chart,
                                          size: 17,
                                          color: Colors.black,
                                        ),
                                      ),
                                      Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.only(
                                                  top: 10, left: 20),
                                              child: AutoSizeText(
                                                'Mass',
                                                style: TextStyle(
                                                    fontSize: 10,
                                                    color: Colors.white,
                                                    fontFamily: 'Lato',
                                                    letterSpacing: 3.0),
                                                minFontSize: 10,
                                                textScaleFactor: 1,
                                                textAlign: TextAlign.justify,
                                                stepGranularity: 1,
                                                maxLines: 1,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                  top: 4, left: 20),
                                              child: AutoSizeText(
                                                projectSnap.data.mass.kg
                                                        .toString() +
                                                    ' kg' +
                                                    " / " +
                                                    projectSnap
                                                        .data.mass.lb
                                                        .toString() +
                                                    ' lb',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.white,
                                                    fontFamily: 'Lato',
                                                    letterSpacing: 3.0),
                                                minFontSize: 10,
                                                textScaleFactor: 1,
                                                textAlign: TextAlign.justify,
                                                stepGranularity: 2,
                                                maxLines: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ]),
                        ),
                      );
                    })
              ],
            ),
          );
        }
      },
      future: getDetailsRocket(widget.idRocket),
    );
  }
}
