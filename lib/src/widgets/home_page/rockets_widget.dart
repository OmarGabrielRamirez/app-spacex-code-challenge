import 'package:app_rockets_spacex/src/pages/details_rocket.dart';
import 'package:app_rockets_spacex/src/service/https/request_rockets.dart';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';

class RocketWidget extends StatefulWidget {
  @override
  _RocketWidgetState createState() => _RocketWidgetState();
}

class _RocketWidgetState extends State<RocketWidget> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      builder: (context, projectSnap) {
        if (projectSnap.connectionState == ConnectionState.none &&
            projectSnap.hasData == null) {
          return Container(
            child: Center(
                child: Column(
              children: <Widget>[
                Text(
                  'Error en el servidor',
                  style: TextStyle(color: Colors.white, letterSpacing: 2.0),
                )
              ],
            )),
          );
        } else if (projectSnap.connectionState == ConnectionState.waiting) {
          return Container(
              child: Center(
                  child: SizedBox(
            width: 30,
            child: LoadingIndicator(
              indicatorType: Indicator.circleStrokeSpin,
              color: Colors.white,
            ),
          )));
        } else if (projectSnap.hasData) {
          return ListView.builder(
            itemCount: projectSnap.data.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return Container(
                color: Colors.black,
                padding:
                    EdgeInsets.only(top: 30, left: 30, bottom: 40, right: 30),
                width: MediaQuery.of(context).size.width - 45,
                child: Card(
                  elevation: 5.0,
                  color: Colors.transparent,
                  child: Stack(
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(40),
                          bottomRight: Radius.circular(40),
                        ),
                        child: CachedNetworkImage(
                          fit: BoxFit.fitHeight,
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height / 2 + 20,
                          imageUrl: projectSnap.data[index].flickrImages[1],
                          placeholder: (context, url) => Container(
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width,
                              height: 20,
                              child: LoadingIndicator(
                                indicatorType: Indicator.semiCircleSpin,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          errorWidget: (context, url, error) => SizedBox(
                            width: MediaQuery.of(context).size.width,
                            height: 20,
                            child: Center(
                              child: Icon(Icons.airplanemode_active),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        top: 466,
                        left: 20,
                        child: InkWell(
                          autofocus: true,
                          onTap: () {
                            Navigator.of(context).push(
                              PageRouteBuilder(
                                pageBuilder:
                                    (context, animation, secondaryAnimation) =>
                                        DetailsRocketPage(
                                  idRocket: projectSnap.data[index].rocketId,
                                ),
                                transitionsBuilder: (context, animation,
                                    secondaryAnimation, child) {
                                  var begin = Offset(0.0, 1.0);
                                  var end = Offset.zero;
                                  var curve = Curves.bounceInOut;
                                  var tween = Tween(begin: begin, end: end)
                                      .chain(CurveTween(curve: curve));
                                  return SlideTransition(
                                    position: animation.drive(tween),
                                    child: child,
                                  );
                                },
                              ),
                            );
                          },
                          child: Container(
                            padding: EdgeInsets.only(top: 3, bottom: 5),
                            child: Text(
                              projectSnap.data[index].rocketName,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 23,
                                  letterSpacing: 3.0),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                          top: 486,
                          left: 20,
                          child: Container(
                            padding: EdgeInsets.only(top: 10, bottom: 5),
                            child: Text(
                              projectSnap.data[index].rocketType.toUpperCase() +
                                  ' - ' +
                                  projectSnap.data[index].company.toUpperCase(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 11,
                                  letterSpacing: 3.0),
                            ),
                          )),
                      Positioned(
                          top: 499,
                          left: 20,
                          child: Container(
                            padding: EdgeInsets.only(top: 10, bottom: 10),
                            child: Text(
                              projectSnap.data[index].country.toUpperCase(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 9,
                                  letterSpacing: 2.0),
                            ),
                          )),
                      Positioned(
                        top: 520,
                        left: 20,
                        right: 10,
                        child: Container(
                          padding: EdgeInsets.only(top: 10),
                          width: MediaQuery.of(context).size.width - 45,
                          child: AutoSizeText(
                            projectSnap.data[index].description,
                            style: TextStyle(
                                fontSize: 11,
                                color: Colors.white,
                                fontFamily: 'Lato',
                                fontWeight: FontWeight.w100,
                                letterSpacing: 2.0),
                            minFontSize: 10,
                            textScaleFactor: 1.2,
                            textAlign: TextAlign.justify,
                            stepGranularity: 2,
                            maxLines: 20,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          );
        }
      },
      future: getAllRocketSpaceX(),
    );
  }
}
