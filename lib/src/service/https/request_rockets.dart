import 'package:app_rockets_spacex/src/models/rocket.dart';
import 'package:app_rockets_spacex/src/utils/environment_var.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';

Future<List<Rocket>> getAllRocketSpaceX() async {
  List<Rocket> rocketList = [];
  var response = await http.get(
    directionToServerApi + "/api/get/all/rockets",
  );

  if (response.statusCode == 200) {
    var jsonResponseRockets = json.decode(response.body);
    for (var x in jsonResponseRockets) {
      Rocket rocket = Rocket.fromJson(x);
      rocketList.add(rocket);
    }
  }
  return rocketList;
}

Future<Rocket> getDetailsRocket(String idRocket) async {
  Rocket rocket;

  var response = await http
      .post(directionToServerApi + "/api/get/details/rocket/" + idRocket);

  if (response.statusCode == 200) {
    var jsonResponseRocket = json.decode(response.body);
    rocket = Rocket.fromJson(jsonResponseRocket);
  }
  return rocket;
}
