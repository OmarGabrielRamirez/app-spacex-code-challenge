
import 'package:app_rockets_spacex/src/widgets/details_page/details_rocket.dart';

import 'package:flutter/material.dart';

class DetailsRocketPage extends StatefulWidget {
  final String idRocket;
  DetailsRocketPage({this.idRocket});
  @override
  _DetailsRocketPageState createState() => _DetailsRocketPageState();
}

class _DetailsRocketPageState extends State<DetailsRocketPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(child: DetailsRocketWidget(idRocket: widget.idRocket)),
    );
  }
}
